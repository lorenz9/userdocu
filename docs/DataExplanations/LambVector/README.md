# Lamb Vector

The Lamb Vector filter computes the Lamb vector $L$ based on the velocity $v$, vorticity $\omega$ and the density $\rho$. 

\begin{equation}
L = \omega \times \boldsymbol{v}  ,
\end{equation}

with vorticity as

\begin{equation}
\omega = \nabla \times \boldsymbol{v}  .
\end{equation}

However, it is possible to compute the Lamb vector only based on the velocity, or on the velocity and the vorticity. If only the velocity is defined
the vorticity is computed internally. 

* The epsilonScaling Parameter scales the radial basis functions used for computing spatial derivatives. It controlls the "smoothness" of the basis function. The smoother the gauss-like surface is, the better the results will be BUT only until a certain number, when the matrix becomes to ill-contioned, which will result in very bada results. Typical values: 1e-1 - 1e-4.
* The kScaling parameter is an optional parameter and defines a constant term that is added to the radial basic function.
* The betaScaling parameter defines the slope of a linear term that is added to the radial basis function.
* The logEps Parameter enables a detailes console output (minimal distance, maximal distance, optimized parameters). Therefore, it should only be used if the spatial derivatives are investigated, because it totally spams the console


```
    <aeroacoustic type="AeroacousticSource_LambVector" inputFilterIds="..." id="...">
      <RBF_Settings epsilonScaling="1e-4"  kScaling="" betaScaling="" logEps=false/>
      <scheme/>
      <targetMesh>
        <hdf5 fileName=..."/>
      </targetMesh>
      <ResultList>
        <velocity resultName="..."/>
        <vorticity/>
        <density />  //NOT WORKING
        <outputQuantity resultName="..."/>
      </ResultList>
      <regions>
        <sourceRegions>
          <region name="..."/>
        </sourceRegions>
        <targetRegions>
          <region name="..."/>
        </targetRegions>
      </regions>
    </aeroacoustic>
```

Note that the density Tag is currently not fully implemented and therefore not working. It is possible to enter results, however it will not alter the 
computed output quantity. 

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [FE-based Interpolation]._
