Analysis Types in openCFS
=========================

All the files in this tutorial can be [`downloaded here`](AnalysisTypes.zip).

Several different analysis types are possible in openCFS.
This is illustated in this tutorial by the example of a solid mechanics problem of a cantilever beam.
In a single openCFS input file different analyses can be collected by defining several `<sequenceStep index="N">` tags, which will run in sequence.

Typical analyses for the mechanic PDE are:

* The *static* analysis computes equilibrium configuration (i.e. the defomation) of the beam for a stationary (static) load.

* The *eigenFrequeny* step computes the natural frequencies and corresponding oscillaion modes of the beam.

* The *harmonic* analysis computes the steady state response of the structure subject to harmonic loading.

* The *transient* step computes the time dependent response of the structure subject to general, time-depedent loading.


```
Sketch of the domain


     CCCCC                                          
    CCCC/C                                          
  CCCC///C                                          
CCCC/////C                                          
CCC//////C                                          
CCC/////8000000000000000000000000000000000000000000008888   ^      ^
CCC///8              cantilever beam                   8    | h   / 
CCC/800000000000000000000000000000000000000000000008888     v    / b
CCC/80000000000000000000000000000000000000000000008888          /
CCC//<----------------------L------------------------>         v
CCC//////C
CCC/////C
CCC////C
CCC///C
CCCC


```

![Sketch of the domain](Sketch-beam.png)

Meshing
------------

1. Type 'trelis -batch -nographics -nojournal beam.jou' on the terminal. 
2. The journal file [`beam.jou`](beam.jou) from (1.) is used as an input.
3. Both an ANSYS-cdb mesh file [`beam.cdb`](beam.cdb) and a Trelis file `beam.trelis` are created.
4. Look into the journal file [`beam.jou`](beam.jou) to see the Trelis commands to create the mesh.
5. These commands could also have been run interactively in Trelis.
6. Look through the mesh file to obtain the beam dimensions.

The file [`beam.cdb`](beam.cdb) was created this way.

Simulation with CFS
---------------

The CFS input file is [`simulation.xml`](simulation.xml).
It contains several analysis steps (static, natural frequency, harmonic, transient).
The material file is called [`mat.xml`](mat.xml) using the usual convention.

To run the simulation execute

```
cfs -p simulation.xml job
```

which creates text files in `history` and the HDF5 output in `results_hdf5/job.cfs`.
Both above steps can be done by running the shell scipt [`run.sh`](run.sh), i.e. in a terminal change to this directory and run
```shell
./run.sh
```

An an exercise:

* Look into the material file to obtain the material properties
* Study the CFS input and get familiar with each analysis step (step definition, BCs, loads, outputs)


Postprocessing
--------------

CFS can write output in various formats.
In this example we define text-based *history* output and *field* output in the HDF5 format (`*.cfs` file).

As an exercise:

1. Look at the produced ASCII file in `history`
2. Load the result in ParaView and try the following things
     * Load the correct analysis step (=multi sequence step)
     * Display deformed geometry (use the *Wrap by Vector* filter)
     * Switch the *Complex Mode* for complex valued results
     * *Animate Harmonic / Modal Results* for the corresponding sequence steps
     * Animate transient results


Furter Sugesstions
-------------------------

* Compare the results with the analytic solution
* Specify a spacially varying load
* Compute the natural frequencies of a free-free beam
* Plot the results in the ASCII file (response of the end-point as a function of time and frequency, respectively)
* What happens if you play with the damping parameter?
* Which effect does the parameter `T` have?
