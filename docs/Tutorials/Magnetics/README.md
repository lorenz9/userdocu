# Permanent Magnet

## Problem Discription

All files in this tutorial can be [`downloaded here`](magnet.zip).

We consider the axi-symmetric geometry (2D model) of a ring-shaped permanent magnet as depicted in the sketch.
The inner radius of the ring $r_1$ is 5 mm, the outer radius $r_2$ is 10 mm, and the height of the ring $l_1$ is 8 mm. 
The size of the simulation domain should be chosen sufficiently large, such that the solution is not significantly influenced by the boundary conditions at the outer edge of the domain.
The ring-shaped permanent magnet is assumed to show a perfect magnetization $M$ of 1.2 T in z-direction. 
The core of the ring is iron. 


![sketch](sketch.png){: style="width:150px"}

## Tasks

1. Create a regular hexaedral mesh using Cubit and [`geometry-ue.jou`](geometry-ue.jou).
2. Simulate the magnet with openCFS and the simulation input [`magnet.xml`](magnet.xml) and material file [`mat.xml`](mat.xml).

## Tutorial Suggestions

* Look at the simulation and try to visualize the magnetic vector potential with the help of [paraview](../../PostProcessing/ParaView/basics.md#visualizing-field-results)
* Try to visualize the magnetic flux density with the help of [paraview](../../PostProcessing/ParaView/basics.md#visualizing-field-results) (Hint: Slice the volume in the middle)
* Look into [`mat.xml`](mat.xml) and see how and what material is defined.
* Change the material of the iron core to air, copper or alu and see how simulation behaves. (Hint: [You can use scripts to automate this](../Scripting/README.md))

## Remarks

* The problem is actuall a 2d-problem, but we use the `<magneticEdge>`-PDE for the simulation, therefore we have model it in 3d (eg. extruding it in z-direction for 1 element) to get a quasi-2d model.
